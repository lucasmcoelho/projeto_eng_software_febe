import pandas as pd
import PySimpleGUI as sg

pd.set_option("display.max_colwidth", None) # não limita o tamanho da coluna no dataframe

class SelecaoDisciplinas:
    def __init__(self, matriz, historico, obrigatorias, optativas, turmas):
        sg.theme("DarkGrey3") 
        sg.SetOptions(element_padding=(2, 5), font="Comic 12")

        periodo = historico.loc[0,'Período']
        coefAbs = historico.loc[0,'Coeficiente Absoluto']
        coefNor = historico.loc[0,'Coeficiente Normalizado']

        tamMatriz = len(matriz)
        equivalentes = {} # criando um dicionário que tem como as chaves todas as disciplinas da matriz curricular e os valores são suas disciplinas equivalentes 
        for i in range(tamMatriz):
            cod = matriz.loc[i, 'Codigo']
            equivalentes[cod] = matriz.loc[matriz['Codigo'] == cod, 'Disciplinas Equivalentes'].to_string(index=False).replace("[", "").replace("]", "").replace("'", "").replace(",", "").split(" ")

        tamObrig = len(obrigatorias)
        for i in range(tamObrig):
            if (obrigatorias.loc[i, 'Situação/Professores'] == 'Aprovado'):
                turmas.drop(turmas.loc[turmas['Codigo'] == obrigatorias.loc[i, 'Cód.']].index)
              
                
                
        

        
        tamTurmas = len(turmas)
        template = []
        for i in range(tamTurmas):
            lin = []
            lin.append(sg.Text(i, size=(5,2)))
            lin.append(sg.Button("", image_filename="btn_sim.png"))
            lin.append(sg.Button("", image_filename="btn_nao.png"))
            lin.append(sg.Text(turmas.loc[i, 'Disciplina'], size=(30,2)))
            lin.append(sg.Text(turmas.loc[i, 'Turma'], size=(10,2)))
            lin.append(sg.Text(turmas.loc[i, 'Professores'].replace("[", "").replace("]", "").replace("'", ""), size=(25,2))) # remove os caracteres [, ] e '
            lin.append(sg.Text(turmas.loc[i, 'Vagas Total'] - turmas.loc[i, 'Vagas Calouros'], size=(5,2)))
            lin.append(sg.Text(turmas.loc[i, 'Horarios'].replace("[", "").replace("]", "").replace("'", ""), size=(25,2))) # remove os caracteres [, ] e '
            template.append(lin)

        self.layout = [
            [sg.Text("Turmas Abertas", font=("Comic 32"))],
            [sg.Text("Selecione as disciplinas que você deseja cursar ou não cursar, nos botões ao lado")], 
            [sg.Text(f"Período: {periodo}"), sg.Text(f"Coeficiente Absoluto: {coefAbs}"), sg.Text(f"Coeficiente Normalizado: {coefNor}")],
            [sg.Text("Nº   Quero/NãoQuero\t\tDisciplina\t\tTurma\tProfessor\t\t\tVagas\t\tHorários", size=(1200,2))],
            [sg.Column(template, scrollable=True, size=(1200, 1000))]
        ]

        self.janela = sg.Window("Seleção de Disciplinas", layout=self.layout, size=(1200, 1000), element_justification=("c"), enable_close_attempted_event=True)

        self.Iniciar()

    def Iniciar(self):
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                break