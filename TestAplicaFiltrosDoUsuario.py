import ManipulacaoInformacoes as mi
import pandas as pd
import unittest

'''
Test Case 2 - aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
O método recebe como entrada uma tabela e dois dicionarios
turmas_abertas_pode_cursar contem todoas as informacoes das turmas abertas das disciplinas que o aluno pode cursar
optativas contém as chaves dos códigos de optativas e um booleano associado a cada chave indicando se é para filtrar ou não
professores contém os nomes dos professores como chave e um booleano associado a cada chave indicando se é para filtrar ou não

em ambos os dicionarios, False indica que a optativa/professor deve remover todas as linhas que possuem esta optativa/professor
'''

class TestAplicaFiltrosDoUsuario(unittest.TestCase):
    
    def test_turmas_abertas_vazia(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
    
        optativas = {'[107]' : True, '[109]' : True}
        
        professores = {'Professor1' : True, 'Professor2' : True}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.empty)
    
    def test_tabela_vazia_quando_ha_somente_optativa_e_as_remove(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '[107]', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '[109]', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
    
        optativas = {'[107]' : False, '[109]' : False}
        
        professores = {'Professor1' : True, 'Professor2' : True}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.empty)
    
    def test_tabela_vazia_quando_remove_todos_professores(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
    
        optativas = {'[107]' : True, '[109]' : True}
        
        professores = {'Professor1' : False, 'Professor2' : False}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.empty)
    
    def test_mantem_turma_quando_turmas_tem_professor_diferente(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC1', 'TUR2', '', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
    
        optativas = {'[107]' : True, '[109]' : True}
        
        professores = {'Professor1' : False, 'Professor2' : True}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertEqual(len(df.index), 1)
        self.assertEqual(df.loc[0, 'Professores'], ['Professor2'])
        
    
    def test_remove_quando_pelo_menos_um_professor_leciona(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1', 'Professor2', 'Professor3'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
    
        optativas = {'[107]' : True, '[109]' : True}
        
        professores = {'Professor1' : True, 'Professor2' : False, 'Professor3' : True}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.empty)
        
    
    def test_nao_altera_se_optativa_nao_existe(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '[107]', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1', 'Professor2', 'Professor3'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '[109]', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
        turmas_abertas_pode_cursar.loc[2] = [1, 'DISC3', 'TUR1', '', 'DISCIPLINA3', 44, '', '', ['3M2', '4M2'], ['Professor3'], ['']]
    
        optativas = {'[107]' : True, '[109]' : True, '[108]' : False}
        
        professores = {'Professor1' : True, 'Professor2' : True, 'Professor3' : True}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.equals(turmas_abertas_pode_cursar))
        
    
    def test_nao_altera_se_professor_nao_existe(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '[107]', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1', 'Professor2', 'Professor3'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '[109]', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
        turmas_abertas_pode_cursar.loc[2] = [1, 'DISC3', 'TUR1', '', 'DISCIPLINA3', 44, '', '', ['3M2', '4M2'], ['Professor3'], ['']]
    
        optativas = {'[107]' : True, '[109]' : True, '[108]' : True}
        
        professores = {'Professor0' : False, 'Professor15' : False, 'Professor20' : False}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.equals(turmas_abertas_pode_cursar))
        
    
    def test_nao_altera_se_optativa_e_professores_vazios(self):
        turmas_abertas_pode_cursar = pd.DataFrame(columns = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas'])
        
        turmas_abertas_pode_cursar.loc[0] = [1, 'DISC1', 'TUR1', '[107]', 'DISCIPLINA1', 44, '', '', ['3M2', '4M2'], ['Professor1', 'Professor2', 'Professor3'], ['']]
        turmas_abertas_pode_cursar.loc[1] = [1, 'DISC2', 'TUR1', '[109]', 'DISCIPLINA2', 44, '', '', ['3M2', '4M2'], ['Professor2'], ['']]
        turmas_abertas_pode_cursar.loc[2] = [1, 'DISC3', 'TUR1', '', 'DISCIPLINA3', 44, '', '', ['3M2', '4M2'], ['Professor3'], ['']]
    
        optativas = {}
        
        professores = {}
        
        df = mi.aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores)
        
        self.assertTrue(df.equals(turmas_abertas_pode_cursar))
               

if __name__ == '__main__':
    unittest.main(verbosity = 3, exit = False) 