# imports desenvolvidas
import Interfaces
import SelecaoHorarios


## "Equivalente" do "int main()" do c
if __name__ == "__main__":
    programa = Interfaces.main() 
    
    tela = 'tela_boas_vindas'
    
    while tela:
        evento = programa.ordem_telas[tela]()
        
        tela = evento


# Para utilizar esta applicação é necessário ter instalado em sua máquina:
# 1   - Um interpretador Python
# 2   - As bibliotecas Numpy, Pandas e Selenium (todas podem ser instaladas utilizando o commando pip)
#     - A instalação do comando pip está fora do escopo deste projeto
#     - Recomenda-se instalar o pacote Anaconda pois já vem com o Python, pip e Spyder (IDE)
# 3   - Um WebDriver de sua escolha adequado para seu SO
#     - O WebDriver escolhido deve estar no PATH do seu SO ou, mais conveniente, no mesmo diretorio deste código
# 3.1 - Chrome: https://sites.google.com/a/chromium.org/chromedriver/downloads
