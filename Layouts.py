import pandas as pd
import PySimpleGUI as sg


def boas_vindas():
    layout = [[sg.Image("logo_utfpr.png", size=(380, 150))],
              [sg.Text("Seja bem-vindo(a), nosso sistema vai gerar horários a partir das disciplinas que faltam cursar.")],
              [sg.Text("Para isso é necessário obter seus dados históricos do Portal do ALuno")],
              [sg.Text("Por favor, selecione o modo de obtenção destes dados")],
              [sg.Button("Automático"), sg.Button("Já obtido e salvo em disco"), sg.Button("Sair")]]
    
    return layout

def orientacao_automatico():
    navegadores = ["Chrome", "Firefox"]
    
    layout = [[sg.Text("Após clicar em começar, seu navegador irá abrir e visitar a página de login do Portal do Aluno")],
              [sg.Text("Realize o login e se necessário nagevegue até que os botões abaixo estejam visíveis")],
              [sg.Image("botoes_portal.PNG", size=(734, 79))],
              [sg.Text("Após o aparecimento dos botões o sistema irá automáticamente obter os dados e fechar o navegador")],
              [sg.Text("Atenção: Durante o obtenção dos dados não interaja com o navegador!", font = ("", 16))],
              [sg.Text("a não ser que o sistema aparente estar travado e demore demais para finalizar :)")],
              [sg.Combo(navegadores, default_value=navegadores[0], size = (14, 14), readonly=True, key = 'navegador'), 
               sg.Checkbox("Salvar os Dados após sua obtenção", default = True, key = 'salvarDados'), 
               sg.Button("Começar"), 
               sg.Button("Cancelar")]]
    
    return layout

def filtro_optativas(optativas, optativas_desejadas):
    header = [[sg.Text("Selecione as categorias de disciplinas optativas que deseja incluir na grade*")]]
    
    checkboxes = [[sg.Checkbox(optativa, default = True if optativas_desejadas == None else optativas_desejadas[optativa], key = optativa, pad = (32, 0))] for optativa in optativas]
        
    footer = [[sg.Text("*Categorias com caixas deasmarcadas irá remover todas as disciplinas desta categoria das turmas abertas")],
              [sg.Button("Anterior"), sg.Button("Próximo")]]
    
    layout = header + [[sg.Column(checkboxes, scrollable = True, size=(500, 300), element_justification = 'l', vertical_scroll_only = True)]] + footer
    
    return layout

def filtro_professores(professores, professores_desejados):
    
    header = [[sg.Text("Selecione os professores que gostaria de ter aula com*")]]
    
    checkboxes = [[sg.Checkbox(professor, default = True if professores_desejados == None else professores_desejados[professor], key = professor, pad = (32, 0))] for professor in professores]
        
    footer = [[sg.Text("*Professores com caixas desmarcadas irão ser removidos das turmas abertas")],
              [sg.Button("Anterior"), sg.Button("Próximo")]]
    
    layout = header + [[sg.Column(checkboxes, scrollable = True, size=(500, 300), element_justification = 'l', vertical_scroll_only = True)]] + footer
    
    return layout

def filtro_disciplinas_garantidas(disciplinas, disciplinas_garantidas):
    
    header = [[sg.Text("Selecione as disciplinas que gostaria de garantir na geração de grade*")]]
    
    checkboxes = [[sg.Checkbox(codigo + ' - ' + disciplina, default = False if disciplinas_garantidas == None or codigo not in disciplinas_garantidas.keys() else disciplinas_garantidas[codigo], key = codigo, pad = (32, 0))] for codigo, disciplina in disciplinas.values]
        
    footer = [[sg.Text("*Não está sendo levado em consideração se as disciplinas selecionas aqui 'batem horário'")],             
              [sg.Button("Anterior"), sg.Button("Próximo")]]
    
    layout = header + [[sg.Column(checkboxes, scrollable = True, size=(500, 300), element_justification = 'l', vertical_scroll_only = True)]] + footer
    
    return layout

def disciplinas_grade(grade):
    title = [[sg.Text("Sua escolha de disciplinas está pronta!", font="Courier 20", pad=(5,25))]]
    
    disciplinas = [[sg.Text(disc['Codigo'], size=(10, 1), pad=(0, 1)), 
                    sg.Text(disc['Disciplina_t'], size=(25, 1), pad=(0, 1)), 
                    sg.Text(disc['Turma'], size=(10, 1), pad=(0, 1)), 
                    sg.Text(disc['Professores'], size=(30, 1), pad=(0, 1))] for i, disc in grade.iterrows()]
    
    disciplinas = [[sg.Text('Codigo', size=(10, 1)), sg.Text('Disciplina', size=(25, 1)), sg.Text('Turma', size=(10, 1)), sg.Text('Professores', size=(30, 1))]] + disciplinas
    
    dict_disc = {horario : disc['Codigo'] for i, disc in grade.iterrows() for horario in disc['Horarios']}
    
    turnos = [turno + str(hora+1) for turno in ['M', 'T', 'N'] for hora in range(6)]
    turnos.pop()
    dias = {'2': 'Segunda (2)', '3': 'Terça (3)', '4': 'Quarta (4)', '5': 'Quinta (5)', '6': 'Sexta (6)', '7': 'Sábado (7)'}

    horarios = [[sg.Text(turno+':', size=(5, 1))]+ [sg.Text(dict_disc[dia+turno] if dia+turno in dict_disc.keys() else '', size=(15, 1), pad=(1, 1), background_color="Grey", text_color="Black") for dia in dias.keys()] for turno in turnos]
    horarios = [[sg.Text('', size=(10, 1))] + [sg.Text(dias[dia], size=(15, 1)) for dia in dias.keys()]] + horarios
    
    footer = [[sg.Button("Anterior", pad=(10,20)), sg.Button("Gerar Novo", pad=(10,20)), sg.Button("Finalizar", pad=(10,20))]]
    
    layout = title + disciplinas + [[sg.HSeparator(pad=(0,30))]] + horarios + footer
    
    return layout

def orientacao_manual():
    layout = [
        [sg.Text("Preencha os campos com suas informações obtidas no portal do aluno (ALFA)!!!!")],
        [sg.Text("Periodo:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Coeficiente Absoluto:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Coeficiente Normalizado:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Disciplinas Obrigatórias:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Disciplinas Optativas:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Matriz Curricular:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Text("Turmas Abertas:", size=(50,0)), sg.Input("", size=(50,0))],
        [sg.Button("Enviar", size=(50,0)), sg.Button("Voltar", size=(50,0))]
    ]

    return layout