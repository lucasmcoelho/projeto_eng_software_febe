<h1 align="center">
  <img alt="image" title="logo" src=".github/logo.png" width=90%/>
</h1>

<p align="center">
  <img alt="License" src="https://img.shields.io/static/v1?label=license&message=MIT&color=32B768&labelColor=000000">

</p>

<br>

## ✨ Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [Python](https://www.python.org/)


## 💻 Projeto

Aplicação para facilitar a escolha das matérias a serem realizadas pelo aluno no período que segue.

## 🚀 Como executar

- Baixe e instale o python >3.6.
- Clone o repositório
- Instale as depedências utilizando `pip install -r requirements.txt` ou `pip3 install -r requirements.txt`
- Inicie seu app com `py main.py` ou `python3 main.py`

## 📄 Licença

Esse projeto está sob a licença maternidade.

