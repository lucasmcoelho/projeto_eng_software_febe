import ManipulacaoInformacoes as mi
import pandas as pd
import unittest

'''
Test Case 1 - filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs):
O método recebe como entrada duas tabelas, o 
cabecalho_historico contendo as informacoes de periodo e coeficientes e as 
disciplinas_cursadas contendo as disciplinas, notas, frequencia e a situação

como saída, o método deve retornar outra tabela com as mesma colunas das disciplinas obrigatorias
porem somente com as linhas contendo apenas uma instancia de cada disciplinas e somente as disciplinas
que foram aprovadas ou atendem os criterios para quebrar os pre-requisitos segundo o regulamente:
    
§ 3º Ficará liberado do pré-requisito para efetivação da matrícula, o estudante que 
já tenha cursado a unidade curricular pré-requisito e tenha coeficiente de 
rendimento normalizado igual ou superior a 0,5 (zero vírgula cinco), 
nota final igual ou superior a 4,0 (quatro) e para as unidades curriculares presenciais 
também tenha obtido frequência mínima de 75% (setenta e cinco por cento).
'''

class TestFiltraDisciplinasAprovadasOuQuebramPre(unittest.TestCase):
    
    def test_multiplas_instancias_com_aprovado(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 00.00, 00.00, 1, 2021, 'Reprovado']
        dis_curs.loc[1] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 03.99, 74.99, 1, 2021, 'Reprovado']
        dis_curs.loc[2] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 04.00, 00.00, 1, 2021, 'Reprovado']
        dis_curs.loc[3] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 00.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[4] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 04.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[5] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 10.00, 99.00, 1, 2021, 'Aprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertEqual(len(df.index), 1)
        self.assertEqual(df.loc[0, 'Situação/Professores'], 'Aprovado')
    
    def test_multiplas_instancias_somente_reprovado(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 00.00, 00.00, 1, 2021, 'Reprovado']
        dis_curs.loc[1] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 03.99, 74.99, 1, 2021, 'Reprovado']
        dis_curs.loc[2] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 04.00, 00.00, 1, 2021, 'Reprovado']
        dis_curs.loc[3] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 00.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[4] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 04.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[5] = [1, 'COD_DISC', 'Disciplina', 'Turma', 'Tipo', 60, 60, 60, 05.00, 80.00, 1, 2021, 'Reprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertEqual(len(df.index), 1)
        self.assertEqual(df.loc[0, 'Situação/Professores'], 'Reprovado')
        self.assertGreaterEqual(df.loc[0, 'Média'], 4.0)
        self.assertGreaterEqual(df.loc[0, 'Freq. (4)'], 75.0)
    
    def test_multiplas_instancias_e_disciplinas(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 00.00, 00.00, 1, 2021, 'Reprovado']
        dis_curs.loc[1] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 03.99, 74.99, 1, 2021, 'Reprovado']
        dis_curs.loc[2] = [1, 'COD_DISC2', 'Disciplina2', 'Turma', 'Tipo', 60, 60, 60, 03.99, 74.99, 1, 2021, 'Reprovado']
        dis_curs.loc[3] = [1, 'COD_DISC2', 'Disciplina2', 'Turma', 'Tipo', 60, 60, 60, 04.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[4] = [1, 'COD_DISC3', 'Disciplina3', 'Turma', 'Tipo', 60, 60, 60, 04.00, 75.00, 1, 2021, 'Reprovado']
        dis_curs.loc[5] = [1, 'COD_DISC3', 'Disciplina4', 'Turma', 'Tipo', 60, 60, 60, 06.00, 75.00, 1, 2021, 'Aprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertEqual(len(df.index), 2)
        self.assertEqual(len(df['Cód.'].unique()), 2)
    
    def test_nenhuma_disciplina(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertTrue(df.empty)
    
    def test_disciplina_nao_quebra_pre_por_nota(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 00.00, 99.00, 1, 2021, 'Reprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertTrue(df.empty)
    
    def test_disciplina_nao_quebra_pre_por_frequencia(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 10.00, 0.00, 1, 2021, 'Reprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertTrue(df.empty)
    
    def test_disciplina_nao_quebra_pre_por_coeficiente(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 0, 0]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 05.99, 99.00, 1, 2021, 'Reprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertTrue(df.empty)
    
    def test_disciplina_aprovada_mais_de_uma_vez(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])        
        
        cab_hist.loc[0] = [10, 1, 1]
        
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 06.00, 75.00, 1, 2021, 'Aprovado']
        
        dis_curs.loc[1] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 10.00, 99.00, 1, 2021, 'Aprovado']
        
        df = mi.filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs)
        
        self.assertEqual(len(df.index), 1)
    
    def test_tabela_cabecalho_historico_vazio(self):
        cab_hist = pd.DataFrame(columns = ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado'])     
        
        dis_curs = pd.DataFrame(columns = ['Semestre', 'Cód.', 'Disciplina', 'Turma', 'Tipo (1)', 
                                           'CHS (2)', 'CHT (3)', 'Créd.', 'Média', 'Freq. (4)', 
                                           'Per. (5)', 'Ano', 'Situação/Professores'])
        
        dis_curs.loc[0] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 06.00, 75.00, 1, 2021, 'Aprovado']
        
        dis_curs.loc[1] = [1, 'COD_DISC1', 'Disciplina1', 'Turma', 'Tipo', 60, 60, 60, 10.00, 99.00, 1, 2021, 'Aprovado']
        
        
        self.assertRaises(KeyError, mi.filtra_disciplinas_aprovadas_ou_quebram_pre, cab_hist, dis_curs)


if __name__ == '__main__':
    unittest.main(verbosity = 3, exit = False)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    