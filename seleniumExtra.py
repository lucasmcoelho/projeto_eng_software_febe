from selenium.webdriver.support import expected_conditions as EC


class element_if_clickable_and_has_attribute_with_value(object):
    def __init__(self, locator, attribute, value):
        self.locator = locator
        self.attribute = attribute
        self.value = value

    def __call__(self, driver):
        for element in EC._find_elements(driver, self.locator):
            if element.get_attribute(self.attribute) == self.value:
                return element

        return False


class element_if_clickable_and_has_text(object):
    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        for element in EC._find_elements(driver, self.locator):
            if element.text == self.text:
                return element

        return False
