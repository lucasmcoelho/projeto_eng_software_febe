import pandas as pd
import PySimpleGUI as sg

class SelecaoHorarios:
    def __init__(self, matriz, historico, obrigatorias, optativas, turmas):
        # Estilo e formatos de layout
        sg.theme("DarkGrey3") 
        horarios = []  # importante ser uma 'matriz' 17 x 1 < sg.Column() >
        for i in range(6):
            lin = []
            lin.append(sg.Button(f"M{i + 1}", size=(5, 1), pad=(0, 6), border_width=0))
            horarios.append(lin)
        for i in range(6):
            lin = []
            lin.append(sg.Button(f"T{i + 1}", size=(5, 1), pad=(0, 6), border_width=0))
            horarios.append(lin)
        for i in range(5):
            lin = []
            lin.append(sg.Button(f"N{i + 1}", size=(5, 1), pad=(0, 6), border_width=0))
            horarios.append(lin)

        tabela = []  # segunda M1 (0,0) -> CEL_0_0 assim por diante até sábado N5 (16, 5) -> CEL_16_5
        for i in range(17):
            lin = []
            for j in range(6):
                lin.append(sg.Text(f"Aula_{i}_{j}", key=f"CEL_{i}_{j}", size=(10, 1), pad=(1, 1), border_width=10, justification=("c"), background_color="grey"))
            tabela.append(lin)

        diasDic = {0: "Segunda (2)", 1: "Terça (3)", 2: "Quarta (4)", 3: "Quinta (5)", 4: "Sexta (6)", 5: "Sábado (7)"}
        dias = []
        for i in range(6):
            dias.append(sg.Button(diasDic[i], size=(10, 0), pad=(0, 0), border_width=0))

        self.layout = [
            [sg.Text("Está é nossa sugestão de horaríos para você:", justification=("c"), font=("Comic 20"))],
            [sg.Text("Horários", size=(12, 0), justification=("c")), dias[0], dias[1], dias[2], dias[3], dias[4], dias[5]],
            [sg.HSeparator()],
            [sg.Column(horarios), sg.VSeparator(), sg.Column(tabela)]
        ]

        self.janela = sg.Window("Seleção de Horários", layout=self.layout, element_justification=("c"), enable_close_attempted_event=True)

        # Configurações 
        self.linhas = []
        for i in range(17):
            self.linhas.append(True)

        self.colunas = []
        for i in range(6):
            self.colunas.append(True)

        self.FormatarDados(matriz, historico, obrigatorias, optativas, turmas)
        self.Iniciar()

    def FormatarDados (self, matriz, historico, obrigatorias, optativas, turmas):
        pass

    def Iniciar(self):
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                break
            elif eventos in ("M1", "M2", "M3", "M4", "M5", "M6"):
                self.TravarLinha(int(eventos[1]))
            elif eventos in ("T1", "T2", "T3", "T4", "T5", "T6"):
                self.TravarLinha(6 + int(eventos[1]))
            elif eventos in ("N1", "N2", "N3", "N4", "N5"):
                self.TravarLinha(12 + int(eventos[1]))
            elif eventos in ("Segunda (2)", "Terça (3)", "Quarta (4)", "Quinta (5)", "Sexta (6)", "Sábado (7)"):
                self.TravarColuna(eventos)

    def TravarLinha(self, linha):
        if self.linhas[linha - 1] == True:
            for i in range(6):
                self.janela[f"CEL_{linha - 1}_{i}"].update("")
        else:
            for i in range(6):
                self.janela[f"CEL_{linha - 1}_{i}"].update(f"Aula_{linha - 1}_{i}")

        self.linhas[linha - 1] = not self.linhas[linha - 1]

    def TravarColuna(self, coluna):
        col = {"Segunda (2)": 0, "Terça (3)": 1, "Quarta (4)": 2, "Quinta (5)": 3, "Sexta (6)": 4, "Sábado (7)": 5}

        if self.colunas[col[coluna]] == True:
            for i in range(17):
                self.janela[f"CEL_{i}_{col[coluna]}"].update("")
        else:
            for i in range(17):
                self.janela[f"CEL_{i}_{col[coluna]}"].update(f"Aula_{i}_{col[coluna]}")

        self.colunas[col[coluna]] = not self.colunas[col[coluna]]