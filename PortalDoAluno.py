import platform
import re
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
import argparse

from seleniumExtra import element_if_clickable_and_has_attribute_with_value, element_if_clickable_and_has_text

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--input', action = 'store', dest = 'ra', required = False, default="", help = 'RA') 
parser.add_argument('-p', '--pass', action = 'store', dest = 'pw', required = False, default="", help = 'PASSWORD') 
arguments = parser.parse_args()

def obtem_web_driver(browser):
    OS = platform.system()

    if OS == "Windows":
        if browser == 'Chrome':
            return webdriver.Chrome(executable_path="chromedriverWINDOWS.exe")
        else:
            return webdriver.Firefox(executable_path="geckodriverWINDOWS.exe")
    elif OS == "Linux":
        if browser == 'Chrome':
            return webdriver.Chrome(executable_path=r"./chromedriverLINUX")
        else:
            return webdriver.Firefox(executable_path=r"./geckodriverLINUX")
    elif OS == "Darwin":
        if browser == 'Chrome':
            return webdriver.Chrome(executable_path=r"./chromedriverMAC")
        else:
            return webdriver.Firefox(executable_path=r"./geckodriverMAC")
    else:
        raise "Sistema Operacional Desconhecido"


class PortalDoAluno():
    def __init__(self, browser):
        # Seções/Botões do Portal do Aluno
        self.__secao_matriz_curricular = 'Matrizes Curriculares'
        self.__secao_historico_completo = 'Histórico Completo'
        self.__secao_turmas_abertas = 'Turmas Abertas'

        self.__MAIN_URL = "https://sistemas2.utfpr.edu.br/login?returnUrl=%2Fdpls%2Fsistema%2Faluno05%2Fmpmenu.inicio"
        self.__DRIVER = obtem_web_driver(browser)
        self.__WAITER = WebDriverWait(self.__DRIVER, 120)
        self.__visita_url(self.__MAIN_URL)
        self.login_automatico()

        self.__WAITER.until(element_if_clickable_and_has_text((By.CLASS_NAME, 'navoff'), self.__secao_matriz_curricular))


    def __visita_url(self, url):
        self.__DRIVER.get(url)

    def fecha_driver(self):
        self.__DRIVER.quit()

    def __clica_secao_portal(self, secao):
        try:
            button = self.__WAITER.until(element_if_clickable_and_has_text((By.CLASS_NAME, 'navoff'), secao))
            button.click()
        except TimeoutException:
            self.fecha_driver()
            raise 'O sistema não encontrou a seção ' + secao + ' da página do portal do aluno após esperar por 2 minutos'

    def __espera_frame_aparecer(self, frame_name = 'if_navega'):
        try:
            frame = self.__WAITER.until(EC.visibility_of_element_located((By.ID, frame_name)))                
            return frame
        
        except TimeoutException:
            self.fecha_driver()
            raise 'Frame não carregou após 2 minutos'

    def __troca_pro_frame_da_secao(self, secao):
        self.__clica_secao_portal(secao)
        
        frame = self.__espera_frame_aparecer()
        self.__DRIVER.switch_to.frame(frame)
        
        if secao == self.__secao_turmas_abertas:
            frame = self.__espera_frame_aparecer('if_listahorario')        
            self.__DRIVER.switch_to.frame(frame)

    def __troca_pro_frame_padrao(self):
        self.__DRIVER.switch_to.default_content()

    def __obtem_linhas_secao_matriz_curricular(self):
        tabelas_matriz = self.__WAITER.until(EC.visibility_of_element_located((By.ID, 'grade')))
        
        return [disciplina.text for tabela_periodo in tabelas_matriz.find_elements(By.TAG_NAME, 'tbody') for disciplina in tabela_periodo.find_elements(By.TAG_NAME, 'tr')]

    def __interpreta_linhas_secao_matriz_currricular(self):
        disciplinas = self.__obtem_linhas_secao_matriz_curricular()
        
        nome_colunas = ['Periodo', 'Optativa', 'Codigo', 'Disciplina', 'Modelo de disciplina', 'Carga Horario Total', 
                        'PreRequisitos', 'Disciplinas Equivalentes', 'CHTs Equivalentes', 'Grupo Equivalentes']
        matriz_curricular_data_frame = pd.DataFrame(columns=nome_colunas)
        
        possiveis_optativa = [' [109] ', ' [107] ', '   ']
        possiveis_modelos = [' NÚCLEO COMUM ', ' HUMANIDADES - NOVA ', ' FORMAÇÃO PROFISSIONAL ', 
                             ' TRABALHO DE CONCLUSÃO ', ' ATIVIDADES COMPLEMENTARES ', 
                             ' ENADE CONCLUINTE ', ' ENADE INGRESSANTE ', ' ESTÁGIO ']
        
        for i, linha in enumerate(disciplinas):
            # separa periodo do resto e obtem optativa   
            periodo = None
            for possivel_optativa in possiveis_optativa:  
                splited = linha.split(possivel_optativa, 1)            
                if len(splited) == 2:                   
                    periodo = int(splited[0])
                    linha = splited[1]                    
                    optativa = possivel_optativa.strip()
                    break
            if periodo == None:
                raise 'Faio'
                
            # Separa codigo da linha do resto
            splited = linha.split('\nTurmas ', 1)  
            if len(splited) == 1:
                raise 'Faio'
            codigo = splited[0]
            linha = splited[1]
            
            # Separa disciplina do resto e obtem Modelo de linha
            disciplina = None
            for possivel_modelo in possiveis_modelos:  
                splited = linha.split(possivel_modelo, 1)            
                if len(splited) == 2:                   
                    disciplina = splited[0]
                    linha = splited[1]                    
                    modelo = possivel_modelo.strip()
                    break
            if disciplina == None:
                raise 'Faio'
                        
            # Ignora horas, obtem cht e separa o resto
            splited = linha.split(' ', 7)            
            cht = int(splited[6])            
            linha = splited[7].split('horas', 1)[1].lstrip()
            
            # Separa todas as palavras
            
            if 'Período:' in linha:
                linha = linha.replace(' ', '\n', 1)
                if linha.endswith('\n'):
                    linha = linha[:-1]
            
            splited = linha.split('\n')
            count = 0
            for valor in reversed(splited):
                if valor.isnumeric() or valor == ' ':
                    count += 1
                else:
                    break
            
            if count % 2 != 0:
                raise "Faio"
            
            nEquivalentes = int(count/2)
            tamanho = len(splited)
            
            pre_requisitos = splited[ : tamanho-3*nEquivalentes]
            pre_requisitos = [pre.replace(' ', '') for pre in pre_requisitos if pre != '']
            disciplinas_equivalentes = splited[tamanho-3*nEquivalentes : tamanho-2*nEquivalentes]
            disciplinas_equivalentes = [disc_equi.replace(' ', '') for disc_equi in disciplinas_equivalentes]   
            cht_equivalentes = splited[tamanho-2*nEquivalentes : tamanho-nEquivalentes]
            cht_equivalentes = [cht_equi.replace(' ', '') for cht_equi in cht_equivalentes]   
            grupo_equivalentes = splited[tamanho-nEquivalentes : ]  
            grupo_equivalentes = [grupo.replace(' ', '') for grupo in grupo_equivalentes]          
                
            matriz_curricular_data_frame.loc[i] = [periodo, optativa, codigo, disciplina, modelo, cht, pre_requisitos, 
                                                   disciplinas_equivalentes, cht_equivalentes, grupo_equivalentes]
            
        
        return matriz_curricular_data_frame

    def obtem_matriz_curricular(self):
        self.__troca_pro_frame_da_secao(self.__secao_matriz_curricular)

        matriz_curricular = self.__interpreta_linhas_secao_matriz_currricular()

        self.__troca_pro_frame_padrao()

        return matriz_curricular

    def __interpreta_secao_historico_completo_cabecalho(self):
        tabelas_cabecalho = self.__WAITER.until(EC.visibility_of_element_located((By.CLASS_NAME, 'cabealuno')))

        nome_colunas = ['Aluno', 'Identidade-UF', 'Data Nascimento', 'Naturalidade', 'Nacionalidade', 'Curso',
                        'Período', 'Turno', 'Matriz', 'Situação', 'Ingresso', 'Coeficiente Absoluto',
                        'Coeficiente Normalizado', 'Data da Colação']

        cabecalho_historico_data_frame = pd.DataFrame(columns=nome_colunas)

        cabecalho_historico = []

        for i in tabelas_cabecalho.find_elements(By.TAG_NAME, 'thead'):
            cabecalho_historico = cabecalho_historico + i.find_elements(By.TAG_NAME, 'strong')

        cabecalho_historico = [i.text for i in cabecalho_historico]

        cabecalho_historico_data_frame.loc[len(cabecalho_historico_data_frame)] = cabecalho_historico

        cabecalho_historico_data_frame['Período'] = int(cabecalho_historico_data_frame['Período'])

        cabecalho_historico_data_frame['Coeficiente Absoluto'] = float(cabecalho_historico_data_frame['Coeficiente Absoluto'].str.replace(',', '.'))

        cabecalho_historico_data_frame['Coeficiente Normalizado'] = float(cabecalho_historico_data_frame['Coeficiente Normalizado'].str.replace(',', '.'))

        return cabecalho_historico_data_frame.loc[:, ['Período', 'Coeficiente Absoluto', 'Coeficiente Normalizado']]

    def __interpreta_secao_historico_completo_disciplinas_cursadas(self, tabela):
        cabecalho_tabela = tabela.find_element(By.TAG_NAME, 'thead')
        nome_colunas = cabecalho_tabela.find_elements(By.TAG_NAME,'th')
        nome_colunas = [i.text for i in nome_colunas]

        tabela_disciplinas = tabela.find_element(By.TAG_NAME, 'tbody')

        disciplinas_cursadas_data_frame = pd.DataFrame(columns = nome_colunas)

        for disciplina in tabela_disciplinas.find_elements(By.TAG_NAME, 'tr'):
            colunas = disciplina.find_elements(By.TAG_NAME, 'td')
            
            colunas = [i.text for i in colunas]
            
            if colunas[0].startswith('(1)') or ',' not in colunas[8]:
                continue
            
            disciplinas_cursadas_data_frame.loc[len(disciplinas_cursadas_data_frame)] = colunas

        disciplinas_cursadas_data_frame['Semestre'] = disciplinas_cursadas_data_frame['Semestre'].astype(int)

        disciplinas_cursadas_data_frame['CHS (2)'] = disciplinas_cursadas_data_frame['CHS (2)'].astype(int)
        
        disciplinas_cursadas_data_frame['CHT (3)'] = disciplinas_cursadas_data_frame['CHT (3)'].astype(int)

        disciplinas_cursadas_data_frame['Média'] = disciplinas_cursadas_data_frame['Média'].str.replace(',', '.').astype(float) 

        disciplinas_cursadas_data_frame['Freq. (4)'] = disciplinas_cursadas_data_frame['Freq. (4)'].str.replace(',', '.').astype(float) 
        
        disciplinas_cursadas_data_frame['Situação/Professores'] = disciplinas_cursadas_data_frame['Situação/Professores'].apply(lambda x: 'Aprovado' if x.startswith('Aprovado') or x.startswith('Crédito') else 'Reprovado' if x.startswith('Reprovado') else x)
        
        return disciplinas_cursadas_data_frame

    def __interpreta_secao_historico_completo_disciplinas_faltantes(self, tabela):
        cabecalho_tabela = tabela.find_element(By.TAG_NAME, 'thead')
        nome_colunas = cabecalho_tabela.find_elements(By.TAG_NAME,'th')
        nome_colunas = [i.text for i in nome_colunas]

        tabela_disciplinas_faltantes = tabela.find_element(By.TAG_NAME, 'tbody')

        disciplinas_faltantes_data_frame = pd.DataFrame(columns = nome_colunas)

        for disciplina in tabela_disciplinas_faltantes.find_elements(By.TAG_NAME, 'tr'):
            colunas = disciplina.find_elements(By.TAG_NAME, 'td')
            colunas = [i.text for i in colunas]

            disciplinas_faltantes_data_frame.loc[len(disciplinas_faltantes_data_frame)] = colunas

        disciplinas_faltantes_data_frame['Semestre'] = disciplinas_faltantes_data_frame['Semestre'].astype(int)
        disciplinas_faltantes_data_frame['Código'] = disciplinas_faltantes_data_frame['Código']
        disciplinas_faltantes_data_frame['Disciplina'] = disciplinas_faltantes_data_frame['Disciplina']

    def obtem_historico_escolar(self):
        self.__troca_pro_frame_da_secao(self.__secao_historico_completo)

        cabecalho_historico = self.__interpreta_secao_historico_completo_cabecalho()
        
        tabela_obrigatorias = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/table/tbody/tr/td/div/center[1]/table/thead/tr/td/table[1]')))
        dis_obrigatorias_cursadas = self.__interpreta_secao_historico_completo_disciplinas_cursadas(tabela_obrigatorias)  

        tabela_optativas = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/table/tbody/tr/td/div/center[2]/table/thead/tr/td/table[1]')))
        dis_optativas_cursadas = self.__interpreta_secao_historico_completo_disciplinas_cursadas(tabela_optativas)

        tabela_faltantes = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/table/tbody/tr/td/div/center[3]/table/thead/tr/td/table[4]')))
        dis_faltantes = self.__interpreta_secao_historico_completo_disciplinas_faltantes(tabela_faltantes)

        self.__troca_pro_frame_padrao()

        return cabecalho_historico, dis_obrigatorias_cursadas, dis_optativas_cursadas, dis_faltantes


    def __obtem_linhas_secao_turmas_abertas(self):
        tabela_turmas_abertas = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/table[2]/tbody')))
        
        return [linha.text for linha in tabela_turmas_abertas.find_elements(By.TAG_NAME, 'tr')]

    def __interpreta_linhas_secao_turmas_abertas(self):
        linhas = self.__obtem_linhas_secao_turmas_abertas()
        
        nome_colunas = ['Codigo', 'Disciplina', 'Turma', 'Vagas Total', 'Vagas Calouros', 'Reserva', 'Prioridade - Curso',
                        'Horarios', 'Professores', 'Optativas']
        turmas_abertas_data_frame = pd.DataFrame(columns=nome_colunas)
        
        iTurmaAberta = 0
        for linha in linhas:
            
            if 'Aulas semanais)' in linha:
                splited = linha.split(' - ', 1)                
                codigo = splited[0]
                disciplina = splited[1].rsplit(' (')[0]
            elif linha.startswith('Turma'):
                continue
            else:
                # Separa turma, vagas abertas e pra calouros e reserva do resto
                splited = linha.split(' ', 4)
                
                turma = splited[0]
                vagas_total = int(splited[1])
                vagas_calouros = int(splited[2])
                reserva = splited[3]   
                linha = splited[4].split(' - ', 1)[1]
                
                # Separa os cursos prioridade do resto
                splited = re.split('[0-9]+ - ', linha)                
                prioridade_curso = [curso.split('\n', 1)[0] for curso in splited]
                linha = splited[len(splited) - 1].split('\n', 1)[1]
                
                # Separa horarios do resto                
                splited = linha.split(') ')                
                horarios = [horario.split('(')[0].replace('- ', '') for horario in splited if '(' in horario]                
                linha = splited[len(splited) - 1].lstrip()
                
                
                # Essa parte é complicada, tanto os professores quanto as optativas estao separados apenas por um \n
                # Pro curso de computação em especifico, há duas alternativas para separar corretamente
                # 1 - Assumir que se optativa é diferente de "Não" a disciplina é ministrada por 1 e somente 1 professor
                # 2 - Assumir que se a optativa é diferente de "Não" então todas as optativas iniciarão com "Engenharia"
                # Se as duas assunções falharem será necessário criar uma lista com todos os professor possíveis 
                # ou todas as optativas possíveis para poder separa-los corretamente
                
                # implementando a 1a opcao
                splits = linha.split('\n')
                
                if splits[len(splits) - 1] == 'Não':
                    professores = splits[:-1]
                    optativas = ['Não']
                else:
                    professores = [splits[0]]
                    optativas = splits[1:]
                
                # Se não há horarios na disciplina não faz sentido poder se matricular, então ignora a turma se estiver vazio
                if horarios:
                    turmas_abertas_data_frame.loc[iTurmaAberta] = [codigo, disciplina, turma, vagas_total, vagas_calouros, reserva,
                                                               prioridade_curso, horarios, professores, optativas]
                iTurmaAberta += 1
        
        return turmas_abertas_data_frame
            
    def __interpreta_secao_turmas_abertas(self):
        tabela_turmas_abertas = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/table[2]/tbody')))
        
        nome_colunas = ['Codigo', 'Disciplina', 'Turma', 'Vagas Total', 'Vagas Calouros', 'Reserva', 'Prioridade - Curso',
                        'Horarios', 'Professor', 'Optativa']
        
        turmas_abertas_data_frame = pd.DataFrame(columns=nome_colunas)
        
        iTurmaAberta = 0
        
        for linha in tabela_turmas_abertas.find_elements(By.TAG_NAME, 'tr'):
            colunas = linha.find_elements(By.TAG_NAME, 'td')
            
            if len(colunas) == 1: # Se na linha só contem a coluna "Codigo - Disciplina (# Aulas Semanais)
                cod_dis_aul = colunas[0].find_element(By.TAG_NAME, 'b').text.split(' - ', 1)
                
                codigo = cod_dis_aul[0]
                
                disciplina = cod_dis_aul[1]
            elif colunas[0].text.startswith('Turma'): # Se na linha tem o cabecalho da turma
                continue
            else: # Se na linha tem as informações da turma
                turma = colunas[0].text
                
                vagas_total = int(colunas[1].text)
                
                vagas_calouros = int(colunas[2].text)
                
                reserva = colunas[3].text
                
                prioridade_curso = colunas[4].text.split(' - ', 1)[1]
                
                horarios = [horario.split('(')[0] for horario in colunas[6].text.split(' - ')]
                
                professor = colunas[7].text
                
                optativa = colunas[8].text
                
                turmas_abertas_data_frame.loc[iTurmaAberta] = [codigo, disciplina, turma, vagas_total, vagas_calouros, 
                                                               reserva, prioridade_curso, horarios, professor, optativa]
                iTurmaAberta += 1
        
        return turmas_abertas_data_frame

    def obtem_turmas_abertas(self):
        self.__troca_pro_frame_da_secao(self.__secao_turmas_abertas)
        
        turmas_abertas = self.__interpreta_linhas_secao_turmas_abertas()
        
        self.__troca_pro_frame_padrao()
        
        return turmas_abertas

    def login_automatico(self):
        if arguments.ra != "" and arguments.pw != "":
            ra = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-login/div/div/p-card/div/div/div/div/form/div[2]/div/input')))
            ra.send_keys(f'{arguments.ra}')

            pas = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-login/div/div/p-card/div/div/div/div/form/div[3]/div/input')))
            pas.send_keys(f'{arguments.pw}')

            click_but = self.__WAITER.until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-login/div/div/p-card/div/div/div/div/form/div[4]/div/button')))
            click_but.click()    
    





















