import numpy as np
import pandas as pd
import ast
import random
from PortalDoAluno import PortalDoAluno

def salva_informacoes(matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas):
    matriz_curricular.to_csv(r'matrizCurricular.csv', index = False)
    cabecalho_historico.to_csv(r'historico.csv', index = False)
    disciplinas_obrigatorias_cursadas.to_csv(r'disciplinasObrigatorias.csv', index = False)
    disciplinas_optativas_cursadas.to_csv(r'disciplinasOptativas.csv', index = False)
    turmas_abertas.to_csv(r'turmasAberta.csv', index = False)

def obtem_informacoes_aluno_automaticamente(browser):
    portal = PortalDoAluno(browser)
    matriz_curricular = portal.obtem_matriz_curricular()
    cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, disciplinas_faltantes = portal.obtem_historico_escolar()
    turmas_abertas = portal.obtem_turmas_abertas()

    portal.fecha_driver()
    
    return matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas
    
def obtem_informacoes_aluno_manualmente():
    # TODO - sprint 5 - obtencao manual das informacoes blalaba
    pass
    # return matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas

def obtem_informacoes_aluno_salvas():
    ## PROVISÓRIO para evitar abrir o sistema toda hora
    matriz_curricular = pd.read_csv(r'matrizCurricular.csv')
    matriz_curricular.fillna('', inplace = True)
    colunas_que_sao_listas = ['PreRequisitos', 'Disciplinas Equivalentes', 'CHTs Equivalentes', 'Grupo Equivalentes']
    for coluna in colunas_que_sao_listas:
        matriz_curricular[coluna] = matriz_curricular[coluna].apply(lambda x: ast.literal_eval(x))
    
    cabecalho_historico = pd.read_csv(r'historico.csv')
    cabecalho_historico.fillna('', inplace = True)
    
    disciplinas_obrigatorias_cursadas = pd.read_csv(r'disciplinasObrigatorias.csv')
    disciplinas_obrigatorias_cursadas.fillna('', inplace = True)
    
    disciplinas_optativas_cursadas = pd.read_csv(r'disciplinasOptativas.csv')
    disciplinas_optativas_cursadas.fillna('', inplace = True)
    
    turmas_abertas = pd.read_csv(r'turmasAberta.csv')
    turmas_abertas.fillna('', inplace = True)
    colunas_que_sao_listas = ['Prioridade - Curso', 'Horarios', 'Professores', 'Optativas']
    for coluna in colunas_que_sao_listas:
        turmas_abertas[coluna] = turmas_abertas[coluna].apply(lambda x: ast.literal_eval(x))

    return matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas

def obtem_informacoes_aluno(modo_obtencao, browser):
    if modo_obtencao == "Automático":
        return obtem_informacoes_aluno_automaticamente(browser)
    elif modo_obtencao == "Manual":
        return obtem_informacoes_aluno_manualmente()
    elif modo_obtencao == "Já obtido e salvo em disco":
        return obtem_informacoes_aluno_salvas()
    else:
        raise f'modo de obtencao {modo_obtencao} invalido'
        

def filtra_disciplinas_aprovadas_ou_quebram_pre(cab_hist, dis_curs):
    dis_curs = dis_curs.copy()
    coeficiente = cab_hist.loc[0, 'Coeficiente Normalizado']
    codigos_unicos = dis_curs['Cód.'].unique()
    
    for codigo in codigos_unicos:
        vezes_cursada = dis_curs.loc[dis_curs['Cód.'] == codigo]
        
        # Se aprovado uma vez, as outras vezes cursadas podem cair fora        
        if 'Aprovado' in vezes_cursada['Situação/Professores'].values:
            outras = vezes_cursada[vezes_cursada['Situação/Professores'] != 'Aprovado'].index
            
        # Se só tem instancias de reprovação, verificar se da pra quebrar o pré
        else:
            # Se o coeficiente normalizado é maior que 0.5, verifica se teve nota e freq >= 4.0 e 75.0 respectivamente
            if coeficiente >= 0.5:
                pre_quebrado = vezes_cursada[(vezes_cursada['Média'] >= 4.0) & (vezes_cursada['Freq. (4)'] >= 75.0)]
                
                # Se tem pelo menos uma que quebrou o pré, remove as outras
                if len(pre_quebrado.index) > 0:
                    outras = np.delete(vezes_cursada.index.values, vezes_cursada.index.values == pre_quebrado.index.values[-1])
                # Se não, remove todas instancias
                else:
                    outras = vezes_cursada.index
            # Se não, remove todas instancias
            else:
                outras = vezes_cursada.index
        dis_curs.drop(outras, inplace = True)
                
    return dis_curs.reset_index(drop = True)
    

def filtra_matriz_mantem_pode_cursar(cab_hist, mat_cur, dis_obg_curs, dis_opt_curs):
    matriz = mat_cur.copy()
    
    cod_obg_aprovadas = dis_obg_curs[dis_obg_curs['Situação/Professores'] == 'Aprovado']['Cód.'].values
    cod_opt_aprovadas = dis_opt_curs[dis_opt_curs['Situação/Professores'] == 'Aprovado']['Cód.'].values    
    cod_aprovadas = np.concatenate((cod_obg_aprovadas, cod_opt_aprovadas))
    
    # Remove da matriz, todas as já aprovadas
    matriz = matriz[~matriz['Codigo'].isin(cod_aprovadas)]
    
    # Remove da matriz, todas as disciplinas de periodo muito maior do que o periodo atual do aluno
    periodo = cab_hist.loc[0, 'Período']   
    periodos_a_frente = 4
    matriz = matriz[matriz['Periodo'] <= periodo + periodos_a_frente]
    
    # Remove da matriz, as disciplinas que o aluno não quebrou pré
    cod_todos_periodos_cursados = np.array(['Período:'+str(i) for i in range(1, periodo+1)])
    cod_todas_obg = dis_obg_curs['Cód.'].values
    cod_todas_opt = dis_opt_curs['Cód.'].values    
    cod_todas = set(np.concatenate((cod_todas_obg, cod_todas_opt, cod_todos_periodos_cursados)))
    
    indexes_a_remover = []
    for index, disciplina in matriz.iterrows():
        
        pres = set(disciplina['PreRequisitos'])
        
        intersection = set.intersection(cod_todas, pres)
        
        # Considerando conjunto de todas as disciplinas cursadas que podem quebrar o pre de outra disciplina
        # e o conjunto de pre-requisitos de uma dada disciplina
        # então a intersecção desses dois conjuntos indicais quais pres foram quebrados
        # e portando se o conjunto interseccao for igual ao conjunto de pre-requisitos, o aluno pode cursar a disciplina
        
        if not pres == intersection:        
            indexes_a_remover.append(index)
    
    matriz.drop(indexes_a_remover, inplace = True)

    return matriz.reset_index(drop = True)
        
def filtra_turmas_abertas_das_disciplinas_que_pode_cursar(tur_abe, mat_pode_cursar):
    cod_tur_abe = set(tur_abe['Codigo'])
    cod_mat_pode_cursar = set(mat_pode_cursar['Codigo'])
    
    # filtra somente o que esta em ambos turmas abertas e pode cursar
    inter = set.intersection(cod_tur_abe, cod_mat_pode_cursar)
    
    ta = tur_abe[tur_abe['Codigo'].isin(inter)]
    mc = mat_pode_cursar[mat_pode_cursar['Codigo'].isin(inter)]
    
    # faz um right join pois as turmas aberta pois codigos duplicados, mas no final das contas é um inner join
    turmas_abertas_pode_cursar = pd.merge(ta, mc, how = 'right', on = 'Codigo', suffixes = ['_t', '_m'])
    
    # colunas_interessadas = ['Codigo', 'Disciplina_t', 'Turma', 'Vagas Total', 'Vagas Calouros',
    #    'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas',
    #    'Periodo', 'Optativa', 'Disciplina_m', 'Modelo de disciplina',
    #    'Carga Horario Total', 'PreRequisitos', 'Disciplinas Equivalentes',
    #    'CHTs Equivalentes', 'Grupo Equivalentes']
    colunas_interessadas = ['Periodo', 'Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Vagas Total',
        'Reserva', 'Prioridade - Curso', 'Horarios', 'Professores', 'Optativas']
    
    return turmas_abertas_pode_cursar.loc[:, colunas_interessadas]
    

def obtem_turmas_das_disciplinas_que_pode_ser_cursada(modo_obtencao, browser = "", salvar = False):
    
    matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas = obtem_informacoes_aluno(modo_obtencao, browser)
    
    if salvar and modo_obtencao != "Já obtido e salvo em disco":
        salva_informacoes(matriz_curricular, cabecalho_historico, disciplinas_obrigatorias_cursadas, disciplinas_optativas_cursadas, turmas_abertas)
        
    dis_obg_curs = filtra_disciplinas_aprovadas_ou_quebram_pre(cabecalho_historico, disciplinas_obrigatorias_cursadas)
    dis_opt_curs = filtra_disciplinas_aprovadas_ou_quebram_pre(cabecalho_historico, disciplinas_optativas_cursadas)
    
    matriz_pode_cursar = filtra_matriz_mantem_pode_cursar(cabecalho_historico, matriz_curricular, dis_obg_curs, dis_opt_curs)
    
    return filtra_turmas_abertas_das_disciplinas_que_pode_cursar(turmas_abertas, matriz_pode_cursar)


def aplica_filtros_do_usuario(turmas_abertas_pode_cursar, optativas, professores):
    ta_filtrada = turmas_abertas_pode_cursar.copy()
    
    # Remove as optativas
    optativas_a_remover = [key for key in optativas.keys() if optativas[key] == False]
    ta_filtrada = ta_filtrada[~ta_filtrada['Optativa'].isin(optativas_a_remover)]
    ta_filtrada.reset_index(drop = True, inplace = True)
    
    # Remove os professores
    professores_a_remover = set([key for key in professores.keys() if professores[key] == False])
    
    indexes_a_remover = []
    for index, disciplina in ta_filtrada.iterrows():
        
        profes = set(disciplina['Professores'])
        
        intersection = set.intersection(professores_a_remover, profes)
        
        # Considerando o conjunto de todos os professores que ministram uma dada disciplina
        # e o conjunto de todos os professores que desaj-se remove
        # então se a intersecção destes dois conjuntos não for vazia, remove-se a disciplina das turmas abertas
        
        if intersection:        
            indexes_a_remover.append(index)
    
    ta_filtrada.drop(indexes_a_remover, inplace = True)
    
    return ta_filtrada.reset_index(drop = True)

def gera_grade_aleatoria(turmas_abertas, disciplinas_garantidas):
    ta = turmas_abertas.copy()
    
    colunas = ['Codigo', 'Turma', 'Optativa', 'Disciplina_t', 'Horarios', 'Professores']
    grade = pd.DataFrame(columns = colunas)
    
    horarios_usados = set([])
    
    disc_garantidas = [key for key in disciplinas_garantidas.keys() if disciplinas_garantidas[key] == True]
    random.shuffle(disc_garantidas)
    
    while disc_garantidas:
        disciplina = disc_garantidas.pop()
        validas = ta[ta['Codigo'] == disciplina]
        
        for index, valida in validas.iterrows():
            horarios = set(valida['Horarios'])
            
            intersection = set.intersection(horarios, horarios_usados)
            
            # Considerando o conjunto dos horarios da disciplina em questao
            # e o conjuntos dos horarios ja utilizado
            # se a interseccao dos conjuntos for vazia, entao pode adicionar a discipina na grade
            
            if not intersection:
                grade.loc[len(grade.index)] = valida[colunas]
                horarios_usados = set.union(horarios_usados, horarios)
                break
            
        ta.drop(validas.index, inplace = True)
    
    disc_restantes = list(set(ta['Codigo'].values))
    random.shuffle(disc_restantes)
    
    while disc_restantes:
        disciplina = disc_restantes.pop()
        validas = ta[ta['Codigo'] == disciplina]
        
        for index, valida in validas.iterrows():
            horarios = set(valida['Horarios'])
            
            intersection = set.intersection(horarios, horarios_usados)
            
            # Considerando o conjunto dos horarios da disciplina em questao
            # e o conjuntos dos horarios ja utilizado
            # se a interseccao dos conjuntos for vazia, entao pode adicionar a discipina na grade
            
            if not intersection:
                grade.loc[len(grade.index)] = valida[colunas]
                horarios_usados = set.union(horarios_usados, horarios)
                break
            
        ta.drop(validas.index, inplace = True)
    
    
    return grade.sort_values('Disciplina_t')












    
    
    