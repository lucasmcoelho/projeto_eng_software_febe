import Layouts as lt
import PySimpleGUI as sg
import ManipulacaoInformacoes as mi

class main():
    def __init__(self):
        self.inicializa_atributos()
        
        self.ordem_telas = {'tela_boas_vindas': self.tela_boas_vindas,
                            'tela_orientacao_automatico': self.tela_orientacao_automatico,
                            'tela_filtro_optativas': self.tela_filtro_optativas,
                            'tela_filtro_professores': self.tela_filtro_professores,
                            'tela_criterio_disciplinas_garantidas': self.tela_criterio_disciplinas_garantidas,
                            'tela_grade': self.tela_grade,
                            'tela_orientacao_manual': self.tela_orientacao_manual}
    
    def inicializa_atributos(self):
        self.turmas_abertas_disciplinas_pode_cursar = None
        self.turmas_abertas_disciplinas_filtradas = None
        self.optativas_desejadas = None 
        self.professores_desejados = None
        self.disciplinas_garantidas = None
    
    def tela_boas_vindas(self): 
        sg.theme("DarkBlack1")
        self.inicializa_atributos()
        
        self.janela = sg.Window("Boas Vindas", layout=lt.boas_vindas(), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            
            if eventos == "Sair" or eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return ''
            elif eventos == "Automático":
                self.janela.close()
                return 'tela_orientacao_automatico'
            elif eventos == "Já obtido e salvo em disco":
                self.janela.close()
                self.turmas_abertas_disciplinas_pode_cursar = mi.obtem_turmas_das_disciplinas_que_pode_ser_cursada(eventos)
                return 'tela_filtro_optativas'  

    def tela_orientacao_automatico(self):
        self.janela = sg.Window("Orientações obtenção automática", layout=lt.orientacao_automatico(), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return ''
            
            elif eventos == "Cancelar":
                self.janela.close()
                return 'tela_boas_vindas'
            
            elif eventos == "Começar":
                self.janela.close()          
                self.turmas_abertas_disciplinas_pode_cursar = mi.obtem_turmas_das_disciplinas_que_pode_ser_cursada("Automático", valores["navegador"], valores["salvarDados"])
                return 'tela_filtro_optativas'
            
    def tela_filtro_optativas(self):
        optativas = set(self.turmas_abertas_disciplinas_pode_cursar['Optativa'].values)
        optativas.remove('')
        
        self.janela = sg.Window("Seleção professores", layout=lt.filtro_optativas(optativas, self.optativas_desejadas), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return ''
            
            elif eventos == "Anterior":
                self.janela.close()  
                self.optativas_desejadas = valores
                return 'tela_boas_vindas'
            
            elif eventos == "Próximo":
                self.janela.close()  
                self.optativas_desejadas = valores
                return 'tela_filtro_professores'           
            
    def tela_filtro_professores(self):
        professores = sorted(set([profe for profes in self.turmas_abertas_disciplinas_pode_cursar['Professores'] for profe in profes]))
        
        self.janela = sg.Window("Seleção professores", layout=lt.filtro_professores(professores, self.professores_desejados), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return ''
            
            elif eventos == "Anterior":
                self.janela.close()  
                self.professores_desejados = valores
                return 'tela_filtro_optativas'
            
            elif  eventos == "Próximo":
                self.janela.close()  
                self.professores_desejados = valores
                self.aplica_filtros() # somente se este for o ultimo filtro
                return 'tela_criterio_disciplinas_garantidas' 

    def aplica_filtros(self):
        self.turmas_abertas_disciplinas_filtradas = mi.aplica_filtros_do_usuario(self.turmas_abertas_disciplinas_pode_cursar, self.optativas_desejadas, self.professores_desejados)  
            
    def tela_criterio_disciplinas_garantidas(self):
        disciplinas = self.turmas_abertas_disciplinas_filtradas[['Codigo', 'Disciplina_t']].drop_duplicates('Codigo').reset_index(drop = True).sort_values('Disciplina_t')
                
        self.janela = sg.Window("Seleção disciplinas garantidas", layout=lt.filtro_disciplinas_garantidas(disciplinas, self.disciplinas_garantidas), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
            
            if eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return ''
            
            elif eventos == "Anterior":
                self.janela.close()  
                self.disciplinas_garantidas = valores
                return 'tela_filtro_professores'
            
            elif  eventos == "Próximo":
                self.janela.close()  
                self.disciplinas_garantidas = valores
                
                return 'tela_grade'
            
    def tela_grade(self):
        grade = mi.gera_grade_aleatoria(self.turmas_abertas_disciplinas_filtradas, self.disciplinas_garantidas)
        
        self.janela = sg.Window("Grade aleatória", layout=lt.disciplinas_grade(grade), size=(800, 900), element_justification=("c"), enable_close_attempted_event=True)
        
        while True:
            eventos, valores = self.janela.Read(timeout=100)
           
            if eventos == "Finalizar" or eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
               self.janela.close()
               return ''
            elif  eventos == "Gerar Novo":
               self.janela.close()  
               return 'tela_grade'
            elif eventos == "Anterior":
               self.janela.close()  
               return 'tela_criterio_disciplinas_garantidas'

    def tela_orientacao_manual(self):
        self.janela = sg.Window("Manual", layout=lt.orientacao_manual(), size=(800, 400), element_justification=("c"), enable_close_attempted_event=True)

        while True:
            eventos, valores = self.janela.Read(timeout=100)
           
            if eventos == "Voltar" or eventos == sg.WIN_CLOSED or eventos == sg.WIN_CLOSE_ATTEMPTED_EVENT:
                self.janela.close()
                return 'tela_boas_vindas'
            
            
            